import React from 'react';
import {render} from 'react-dom';
import App from './App.jsx';
import enLocaleData from './i18n/locales/en/english.json';
import esLocaleData from './i18n/locales/es/spanish.json';
import {IntlProvider} from 'react-intl';

const DEFAULT_LOCALE = 'en';
const rootElement = document.getElementById('root');

const localeLanguageFiles = {
    es: esLocaleData,
    en: enLocaleData
};

const getLocaleLanguage = () => {
    const currentLocaleLanguage = localStorage.getItem('currentLocaleLanguage');

    if (currentLocaleLanguage === null) return DEFAULT_LOCALE;
    return currentLocaleLanguage;
}

const startApp = (localeFiles) => {
    render(
        <IntlProvider locale={getLocaleLanguage()} messages={localeFiles[getLocaleLanguage()]}>
            <App/>
        </IntlProvider>,
        rootElement,
    );
};

startApp(localeLanguageFiles);
