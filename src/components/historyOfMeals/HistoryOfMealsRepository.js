import ConstantURLs from '../../api_url'

export default function HistoryOfMealsRepository() {

    const getHistoryMeals = (page, size, username) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/findHistoryMealsPageable?page=' + page + '&size=' + size + '&username=' + username)
            .then(response => response.json());

    const getHistoryMealByNameAndLastDay = (page, size, mealName, mealLastDay, username) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/findHistoryMealByNameAndLastDayPageable?page=' + page + '&size=' + size +
            '&mealName=' + mealName + '&mealLastDay=' + mealLastDay + '&username=' + username)
            .then(response => response.json());

    return {getHistoryMeals, getHistoryMealByNameAndLastDay}
}