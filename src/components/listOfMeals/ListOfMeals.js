import React from 'react';
import ListOfMeals from './ListOfMeals.jsx';

export default ({...props}) => <ListOfMeals {...props} />;
