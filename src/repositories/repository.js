// import ConstantsUrl from '../../constants'
//
// export default function GameStatisticsRepository() {
//
//     const getGameProductsBy = (country, gameMode) => fetch(`${ConstantsUrl.API_URL_LOCAL}/products?country=${country}&gameMode=${gameMode}`)
//         .then(response => response.json());
//
//     const referralCodes = () => fetch(`${ConstantsUrl.API_URL_LOCAL}/app-config`)
//         .then(response => response.json());
//
//     const updateReferralCode = (code, country) => fetch(`${ConstantsUrl.API_URL_LOCAL}/update-referral-code?code=${code}&country=${country}`,
//         {
//             method: 'POST',
//         })
//         .then();
//
//     const updateProduct = (gameMode, gameRegion, id, product, price, productLink, imageLink) =>
//         fetch(
//             `${ConstantsUrl.API_URL_LOCAL}/update-product?gameMode=${gameMode}&gameRegion=${gameRegion}&id=${id}&product=${product}&price=${price}&productLink=${productLink}&imageLink=${imageLink}`,
//             {
//                 method: 'POST',
//             }
//         )
//             .then();
//
//     const createProduct = (gameMode, gameRegion, product, price, productLink, imageLink) =>
//         fetch(
//             `${ConstantsUrl.API_URL_LOCAL}/create-product?gameMode=${gameMode}&gameRegion=${gameRegion}&product=${product}&price=${price}&productLink=${productLink}&imageLink=${imageLink}`,
//             {
//                 method: 'POST',
//             }
//         )
//             .then();
//
//     const deleteProduct = (gameRegion, id) =>
//         fetch(
//             `${ConstantsUrl.API_URL_LOCAL}/delete-product?gameRegion=${gameRegion}&id=${id}`,
//             {
//                 method: 'POST',
//             }
//         )
//             .then();
//
//     const messages = () => fetch(`${ConstantsUrl.API_URL_LOCAL}/admin-messages`)
//         .then(response => response.json());
//
//     const updateMessage = (key, value, id) =>
//         fetch(
//             `${ConstantsUrl.API_URL_LOCAL}/update-message?key=${key}&value=${value}&id=${id}`,
//             {
//                 method: 'POST',
//             }
//         )
//             .then();
//
//     const createMessage = (gameRegion, key, value) =>
//         fetch(
//             `${ConstantsUrl.API_URL_LOCAL}/create-message?gameRegion=${gameRegion}&key=${key}&value=${value}`,
//             {
//                 method: 'POST',
//             }
//         )
//             .then();
//
//
//     const deleteMessage = (id) =>
//         fetch(
//             `${ConstantsUrl.API_URL_LOCAL}/delete-message?id=${id}`,
//             {
//                 method: 'POST',
//             }
//         )
//             .then();
//
//     const login = (user, password) =>
//         fetch(
//             `${ConstantsUrl.API_URL_LOCAL}/login?userName=${user}&password=${password}`,
//             {
//                 method: 'POST',
//             }
//         )
//             .then(res => res);
//
//     return {
//         getGameProductsBy,
//         referralCodes,
//         updateReferralCode,
//         updateProduct,
//         createProduct,
//         deleteProduct,
//         messages,
//         updateMessage,
//         createMessage,
//         deleteMessage,
//         login
//     };
// }
//
//
