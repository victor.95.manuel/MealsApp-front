import ConstantURLs from '../../api_url'

export default function CreateMealRepository() {

    const saveMeal = (mealToSave) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/meal', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(mealToSave),
        }).then();

    return {saveMeal}
}
