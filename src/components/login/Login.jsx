import './Login.css';
import React, {useState, useEffect} from 'react';
import {useHistory} from "react-router-dom";
import LoginRepository from "./LoginRepository";
import {toast, Toaster} from "react-hot-toast";
import {FormattedMessage} from 'react-intl';
import {useIntl} from 'react-intl';

export default (props) => {

    const LOGIN_SUCCESS = "LoginSuccess";
    const HOME = "/home"
    const LOGIN = '/'
    const SPANISH_LOCALE = 'es';
    const ENGLISH_LOCALE = 'en';
    const [userName, setUserName] = useState('');
    const IMAGES_URL = '/statics/';

    const history = useHistory();
    const intl = useIntl();

    const routeToHome = () => {
        localStorage.setItem('isUserLogged', '1');
        localStorage.setItem('userLoggedName', userName);

        toast.dismiss();
        history.push(HOME);
    }

    const onChangeUsername = (username) => {
        setUserName(username.target.value);
    }

    const showLoginUserError = (loginError) => {
        toast.error(intl.formatMessage({id: 'login.usernameError'}));
        document.getElementsByClassName("loginInput").item(0).style.boxShadow = "0 0 0 0.25rem red";
    }

    const updateErrors = (jsonToUpdate) => {
        if (jsonToUpdate === null) return;

        jsonToUpdate.map(
            (jsonElement) => {
                switch (jsonElement.element.className) {
                    case "MealUserName":
                        showLoginUserError();
                        break;
                    case "MealLogin":
                        showLoginUserError();
                        cleanUsername();
                        break;
                    default:
                        break;
                }
            }
        );

        toast.error(intl.formatMessage({id: 'login.error'}));
    }

    const cleanErrors = () => {
        document.getElementsByClassName("loginInput").item(0).style.boxShadow = "none";
    }

    const cleanUsername = () => {
        document.getElementsByClassName("loginInput").item(0).value = null;
        setUserName('');
    }

    const loginUser = () => {
        cleanErrors();

        LoginRepository()
            .getLoginUser(userName)
            .then((response) => {
                const loginResponse = response[0].element;

                if (loginResponse.className !== LOGIN_SUCCESS) updateErrors(response);
                else routeToHome();
            });
    }

    const setLocaleToSpanish = () => {
        localStorage.setItem('currentLocaleLanguage', SPANISH_LOCALE);
        location.reload();
    }

    const setLocaleToEnglish = () => {
        localStorage.setItem('currentLocaleLanguage', ENGLISH_LOCALE);
        location.reload();
    }

    const renderLoginTitle = () => <div className="loginBody">
        <img className="loginAppIcon"
             src={IMAGES_URL + 'mealsAppIcon.png'}
             alt="MealsApp Icon"/>
    </div>;

    const renderLoginSection = () => <div>
        <table>
            <tbody>
            <tr>
                <td className="loginTd">
                    {/*<label>*/}
                    {/*    <FormattedMessage id='login.username' defaultMessage='Username'/>*/}
                    {/*</label>*/}
                    <input className="form-control loginInput"
                           placeholder={intl.formatMessage({id:'login.usernamePlaceholder'})}
                           onChange={onChangeUsername}/>
                </td>
            </tr>
            <tr>
                <td className="loginCenterTd">
                    <button className="btn btn-outline-dark loginButton"
                            onClick={() => loginUser()}>
                        <p className="loginButtonText">
                            <FormattedMessage id='login.loginButton' defaultMessage='Login'/>
                        </p>
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>;

    const renderLoginToast = () => <div>
        <Toaster
            position="top-left"
            reverseOrder={false}
        />
    </div>;

    const renderLanguageSection = () => <table>
        <tbody>
        <tr>
            <td className="loginRightTd">
                <button className="btn btn-outline-secondary loginLanguageSelection loginLeftLanguageButton"
                        onClick={() => setLocaleToSpanish()}>
                    <img className="loginLanguageButton" src={IMAGES_URL + 'spanishIcon.png'}
                         alt="Spanish Icon"/>
                </button>
            </td>
            <td className="loginLeftTd">
                <button className="btn btn-outline-secondary loginLanguageSelection loginRightLanguageButton"
                        onClick={() => setLocaleToEnglish()}>
                    <img className="loginLanguageButton" src={IMAGES_URL + 'englishIcon.png'}
                         alt="English Icon"/>
                </button>
            </td>
        </tr>
        </tbody>
    </table>;

    return (<React.Fragment>
            <div className={'container h-100'}>
                <div className="row h-100 justify-content-center">
                    <div className={'loginHeader'}/>
                    <div className="row justify-content-center align-items-end">
                        {renderLoginTitle()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderLanguageSection()}
                    </div>
                    <div className="row justify-content-center align-items-start">
                        {renderLoginSection()}
                    </div>
                    <div className={'loginFooter'}/>
                </div>
            </div>
            {renderLoginToast()}
        </React.Fragment>
    );
}
