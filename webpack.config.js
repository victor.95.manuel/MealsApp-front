const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    // APP ENTRY POINT
    entry: path.join(__dirname, 'src', 'index.js'),

    // OUTPUT DIRECTORY
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].[contenthash].js'
    },

    // EVIROMENT MODE
    mode: process.env.NODE_ENV || 'development',

    // LOADERS
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },

    // PATH RESOLVE
    resolve: {
        extensions: ['.js', '.json', '.jsx'],

        modules: [
            path.resolve(__dirname, 'src'),
            'node_modules'
        ]
    },

    // // DEV SERVER ENTRY POINT
    devServer: {
        contentBase: path.resolve(__dirname, "./src"),
        port: 3000,
        watchContentBase: true,
        open: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './src/index.html'),
        })
    ]
};
