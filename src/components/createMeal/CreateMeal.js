import React from 'react';
import CreateMeal from './CreateMeal.jsx';

export default ({...props}) => <CreateMeal {...props} />;
