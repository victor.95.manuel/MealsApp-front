import React from 'react';
import HistoryOfMeals from './HistoryOfMeals.jsx';

export default ({...props}) => <HistoryOfMeals {...props} />;
