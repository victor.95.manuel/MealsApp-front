import './Home.css';
import React, {useState, useEffect} from 'react';
import {useHistory} from "react-router-dom";
import HomeRepository from "./HomeRepository";
import {FormattedMessage} from "react-intl";
import {toast} from "react-hot-toast";

export default (props) => {

    const CREATE_MEAL = "/createMeal"
    const LIST_OF_MEALS = "/listOfMeals"
    const HISTORY_OF_MEALS = "/historyOfMeals"
    const GET_MEAL = "/getMeal"
    const LOGIN = '/'
    const IMAGES_URL = '/statics/';

    const history = useHistory();

    useEffect(() => {
        checkUserLogged();
    }, [props.reload]);

    const routeToCreateMeal = () => {
        toast.dismiss();
        history.push(CREATE_MEAL);
    }

    const routeToListOfMeals = () => {
        toast.dismiss();
        history.push(LIST_OF_MEALS);
    }

    const routeToHistoryOfMeals = () => {
        toast.dismiss();
        history.push(HISTORY_OF_MEALS);
    }

    const routeToGetMeal = () => {
        toast.dismiss();
        history.push(GET_MEAL);
    }

    const routeToLogin = () => {
        localStorage.setItem('isUserLogged', '0');
        localStorage.setItem('userLoggedName', '');
        toast.dismiss();

        history.push(LOGIN);
    }

    const checkUserLogged = () => {
        const isUserLogged = localStorage.getItem('isUserLogged');
        const userLoggedName = localStorage.getItem('userLoggedName');

        if (isUserLogged === null ||
            isUserLogged.length === 0 ||
            isUserLogged === '0' ||
            userLoggedName === null ||
            userLoggedName.length === 0)
            routeToLogin();
    }

    const getUsername = () => {
        const isUserLogged = localStorage.getItem('isUserLogged');
        if (isUserLogged === '1') return localStorage.getItem('userLoggedName');

        routeToLogin();
    }

    const renderHomeTitle = () => <div className="homeBody">
        <div className="homeBody">
            <div className='homeUsernameDiv'>
                <p className='homeUsernameText'>
                    <FormattedMessage id='home.welcome' defaultMessage='Welcome'/>
                </p>
                <p className='homeUsernameText'>
                    <strong>
                        {getUsername()}
                    </strong>
                </p>
            </div>

            <img className="mealsAppIcon"
                 src={IMAGES_URL + 'mealsAppIcon.png'}
                 alt="MealsApp Icon"/>
        </div>
    </div>;

    const renderHomeSection = () => <div>
        <table className="homeTable">
            <tbody>
            <tr>
                <td className="homeTd">
                    <button className="btn btn-outline-dark homeButton"
                            onClick={() => routeToGetMeal()}>
                        <p className="homeButtonText">
                            <FormattedMessage id='home.getMeal' defaultMessage='Get Meal'/>
                        </p>
                    </button>
                </td>
            </tr>
            <tr>
                <td className="homeTd">
                    <button className="btn btn-outline-dark homeButton"
                            onClick={() => routeToCreateMeal()}>
                        <p className="homeButtonText">
                            <FormattedMessage id='home.createMeal' defaultMessage='Create Meal'/>
                        </p>
                    </button>
                </td>
            </tr>
            <tr>
                <td className="homeTd">
                    <button className="btn btn-outline-dark homeButton"
                            onClick={() => routeToListOfMeals()}>
                        <p className="homeButtonText">
                            <FormattedMessage id='home.listOfMeals' defaultMessage='List of Meals'/>
                        </p>
                    </button>
                </td>
            </tr>
            <tr>
                <td className="homeTd">
                    <button className="btn btn-outline-dark homeButton"
                            onClick={() => routeToHistoryOfMeals()}>
                        <p className="homeButtonText">
                            <FormattedMessage id='home.historyOfMeals' defaultMessage='History of Meals'/>
                        </p>
                    </button>
                </td>
            </tr>
            <tr>
                <td className="homeTd">
                    <button className="btn btn-outline-dark homeButton"
                            onClick={() => routeToLogin()}>
                        <p className="homeButtonText">
                            <FormattedMessage id='home.logout' defaultMessage='Logout'/>
                        </p>
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>;

    return (<React.Fragment>
            <div className={'container h-100'}>
                <div className="row h-100 justify-content-center">
                    <div className={'homeHeader'}/>
                    <div className="row justify-content-center align-items-center">
                        {renderHomeTitle()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderHomeSection()}
                    </div>
                    <div className={'homeFooter'}/>
                </div>
            </div>
        </React.Fragment>
    );
}
