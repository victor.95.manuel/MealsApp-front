import './ListOfMeals.css';
import React, {useState, useEffect} from 'react';
import {useHistory} from "react-router-dom";
import ListOfMealsRepository from "./ListOfMealsRepository";
import Modal from 'react-modal';
import {toast, Toaster} from "react-hot-toast";
import {FormattedMessage, useIntl} from "react-intl";

export default (props) => {

    const DEFAULT_PAGE_NUMBER = 0;
    const DEFAULT_SIZE_PAGE = 4;
    const DEFAULT_PAGE_SIZE = 3;
    const MEAL_TYPE = ["Lunch", "Dinner"];
    const MAX_SIZE_PAGE = 10;
    const SUCCESS_VALUE = "DomainSuccess"
    const BACK_TO_HOME = "/home"
    const LOGIN = "/"
    const ENGLISH_LOCALE = 'en';
    const SPANISH_LOCALE = 'es';
    const LUNCH = 'Lunch';
    const DINNER = 'DINNER';
    const IMAGES_URL = '/statics/';

    const [allMeals, setAllMeals] = useState([]);
    const [currentPageNumber, setCurrentPageNumber] = useState(null);
    const [listOfPages, setListOfPages] = useState([]);
    const [nameFilter, setNameFilter] = useState("");
    const [typeFilter, setTypeFilter] = useState("");
    const [lastDayFilter, setLastDayFilter] = useState("");
    const [emptyCheckBox, setEmptyCheckBox] = useState(false);
    const [currentSizePage, setCurrentSizePage] = useState(DEFAULT_SIZE_PAGE);
    const [mealNameModal, setMealNameModal] = useState("");
    const [mealTypeModal, setMealTypeModal] = useState("");
    const [mealLastDayModal, setMealLastDayModal] = useState("");
    const [mealPeriodicityModal, setMealPeriodicityModal] = useState("");
    const [mealIdModal, setMealIdModal] = useState("");
    const [mealNameModalError, setMealNameModalError] = useState("");
    const [mealTypeModalError, setMealTypeModalError] = useState("");
    const [mealPeriodicityModalError, setMealPeriodicityModalError] = useState("");
    const [editModalIsOpen, setEditModalIsOpen] = useState(false);
    const [resetModalIsOpen, setResetModalIsOpen] = useState(false);
    const [deleteModalIsOpen, setDeleteModalIsOpen] = useState(false);
    const [mealToReset, setMealToReset] = useState("");
    const [mealToDelete, setMealToDelete] = useState("");
    const [username, setUsername] = useState('');

    const history = useHistory();
    const intl = useIntl();

    const checkUserLogged = () => {
        const isUserLogged = localStorage.getItem('isUserLogged');
        const userLoggedName = localStorage.getItem('userLoggedName');

        if (isUserLogged === null ||
            isUserLogged.length === 0 ||
            isUserLogged === '0' ||
            userLoggedName === null ||
            userLoggedName.length === 0)
            routeToLogin();
    }

    const routeToLogin = () => {
        localStorage.setItem('isUserLogged', '0');
        localStorage.setItem('userLoggedName', '');
        toast.dismiss();

        history.push(LOGIN);
    }

    const getTypeOptions = () => {
        const locale = localStorage.getItem('currentLocaleLanguage');

        if (locale === null || locale.length === 0)
            return [
                {label: "", value: ""},
                {label: "Lunch", value: "LUNCH"},
                {label: "Dinner", value: "DINNER"}
            ];

        switch (locale) {
            case ENGLISH_LOCALE:
                return [
                    {label: "", value: ""},
                    {label: "Lunch", value: "LUNCH"},
                    {label: "Dinner", value: "DINNER"}
                ];
            case SPANISH_LOCALE:
                return [
                    {label: "", value: ""},
                    {label: "Comida", value: "LUNCH"},
                    {label: "Cena", value: "DINNER"}
                ];
            default:
                return [
                    {label: "", value: ""},
                    {label: "Lunch", value: "LUNCH"},
                    {label: "Dinner", value: "DINNER"}
                ];
        }
    }

    const formatDate = (date) => {
        if (date === null || date.length === 0) return null;

        let dateSplit = formatDateOffset(date).toISOString().split('-')
        let year = dateSplit[0];
        let month = dateSplit[1];
        let day = dateSplit[2].substring(0, 2);

        return day.concat("/").concat(month).concat("/").concat(year);
    }

    const formatDateOffset = (date) => {
        if (date === null || date.length === 0) return null;

        let newDate = new Date(date);
        let timeZoneFromDB = 4.00;
        let timeZoneDifference = timeZoneFromDB * 60 + newDate.getTimezoneOffset();
        return new Date(newDate.getTime() + timeZoneDifference * 60 * 1000);
    }

    const setupPageableList = (pageable) => {
        setAllMeals(pageable.pageableList);
        setCurrentPageNumber(pageable.currentPageNumber + 1);

        let pagesArray = new Array(pageable.pagesNumber);
        let minPageToShow = Math.max(0, pageable.currentPageNumber - DEFAULT_PAGE_SIZE);
        let maxPageToShow = Math.min(pagesArray.length, pageable.currentPageNumber + DEFAULT_PAGE_SIZE);

        for (let i = minPageToShow; i < maxPageToShow; i++)
            pagesArray[i] = i + 1;

        setListOfPages(pagesArray);
    }

    const getButtonClassName = (pageNumber) => {
        if (pageNumber === currentPageNumber) return "listOfMealCurrentPageNumberButton";
        else return "listOfMealPageNumberButton";
    }

    const getPagedMeals = (page) => {
        if (nameFilter.length === 0 && typeFilter.length === 0 && lastDayFilter.length === 0 && !emptyCheckBox)
            return getPageAllMeals(page);

        return getPageMealsByNameAndTypeAndLastDay(page, nameFilter, typeFilter, lastDayFilter, emptyCheckBox);
    }

    const getPageAllMeals = (page) => {
        ListOfMealsRepository()
            .getAllMeals(page - 1, currentSizePage, username)
            .then(setupPageableList);
    }

    const getPageMealsByNameAndTypeAndLastDay = (page, mealName, mealType, mealLastDay, emptyCheckBox) => {
        ListOfMealsRepository()
            .getMealByNameAndTypeAndLastDay(page - 1, currentSizePage, mealName, mealType, mealLastDay, emptyCheckBox, username)
            .then(setupPageableList);
    }

    const getPageButtons = () => {
        return listOfPages.map((page) => <button key={page}
                                                 className={getButtonClassName(page) + " btn btn-outline-dark listOfMealButton listOfMealPageButton"}
                                                 onClick={() => getPagedMeals(page)}>
            {page}
        </button>);
    }

    const getAllMeals = () => {
        ListOfMealsRepository()
            .getAllMeals(DEFAULT_PAGE_NUMBER, currentSizePage, username)
            .then(setupPageableList);
    }

    const getAllMealsInit = () => {

        if (localStorage.getItem('isUserLogged') !== '1') return;

        const currentUsername = localStorage.getItem('userLoggedName');

        ListOfMealsRepository()
            .getAllMeals(DEFAULT_PAGE_NUMBER, currentSizePage, currentUsername)
            .then(setupPageableList);
    }

    const searchByFilter = (page) => {
        if (nameFilter.length === 0 && typeFilter.length === 0 && lastDayFilter.length === 0 && !emptyCheckBox)
            return getAllMeals();

        return ListOfMealsRepository()
            .getMealByNameAndTypeAndLastDay(page, currentSizePage, nameFilter, typeFilter, lastDayFilter, emptyCheckBox, username)
            .then(setupPageableList);
    }

    const onNameFilterInput = (e) => {
        setNameFilter(e.target.value);
    }

    const onTypeFilterSelect = (e) => {
        setTypeFilter(e.target.value);
    }

    const onLastDayFilterSelect = (e) => {
        setLastDayFilter(e.target.value);
    }

    const getListOfSizePage = () => {
        let arraySizePage = new Array(MAX_SIZE_PAGE);
        for (let i = 0; i < MAX_SIZE_PAGE; i++)
            arraySizePage[i] = i + 1;

        return arraySizePage;
    }

    const onSizePageChange = (e) => {
        setCurrentSizePage(e.target.value);
    }

    const getFormattedMealType = (mealType) => {
        switch (mealType) {
            case LUNCH:
                return intl.formatMessage({id: 'listOfMeals.mealTypeLunchTable'});
            case DINNER:
                return intl.formatMessage({id: 'listOfMeals.mealTypeDinnerTable'});
            default:
                return intl.formatMessage({id: 'listOfMeals.mealTypeLunchTable'});
        }
    }

    const onEmptyCheckBoxChange = () => {
        setEmptyCheckBox(!emptyCheckBox);
    }

    const cleanErrors = () => {
        setMealNameModalError(null);
        setMealTypeModalError(null);
        setMealPeriodicityModalError(null);

        document.getElementsByClassName("modalNameInput").item(0).style.boxShadow = "none";
        document.getElementsByClassName("modalTypeInput").item(0).style.boxShadow = "none";
        document.getElementsByClassName("modalPeriodicityInput").item(0).style.boxShadow = "none";
    }

    const showEditModalMeal = (meal) => {
        openEditModal();

        setMealNameModal(meal.mealName);
        setMealTypeModal(meal.mealType);
        setMealLastDayModal(meal.mealLastDay);
        setMealPeriodicityModal(meal.mealPeriodicity);

        ListOfMealsRepository()
            .getMealByNameAndTypeAndLastDay(0, 1, meal.mealName, meal.mealType, "", false, username)
            .then((meal) => setMealIdModal(meal.pageableList[0].mealid));
    }

    const closeEditMeal = () => {
        getPagedMeals(currentPageNumber);
        closeEditModal();
        toast.success(intl.formatMessage({id: 'listOfMeals.editModalSuccess'}));
    }

    const showMealNameError = () => {
        toast.error(intl.formatMessage({id: 'listOfMeals.mealNameErrorEditModal'}));
        document.getElementsByClassName("modalNameInput").item(0).style.boxShadow = "0 0 0 0.25rem red";
    }

    const showMealTypeError = () => {
        toast.error(intl.formatMessage({id: 'listOfMeals.mealTypeErrorEditModal'}));
        document.getElementsByClassName("modalTypeInput").item(0).style.boxShadow = "0 0 0 0.25rem red";
    }

    const showMealPeriodicityError = () => {
        toast.error(intl.formatMessage({id: 'listOfMeals.mealPeriodicityErrorEditModal'}));
        document.getElementsByClassName("modalPeriodicityInput").item(0).style.boxShadow = "0 0 0 0.25rem red";
    }

    const updateErrors = (jsonToUpdate) => {
        if (jsonToUpdate === null) return;

        for (let i = 0; i < jsonToUpdate.length; i++) {
            switch (jsonToUpdate[i].element.className) {
                case "MealName":
                    showMealNameError();
                    break;
                case "MealType":
                    showMealTypeError();
                    break;
                case "MealPeriodicity":
                    showMealPeriodicityError();
                    break;
                default:
                    break;
            }
        }

        toast.error(intl.formatMessage({id: 'listOfMeals.errorEditModal'}));
    }

    const updateEditMeal = () => {
        const mealToUpdate = {
            'mealId': mealIdModal,
            'mealName': mealNameModal,
            'mealType': mealTypeModal,
            'mealPeriodicity': mealPeriodicityModal,
            'mealLastDay': formatDateOffset(mealLastDayModal),
            'username': username
        };

        cleanErrors();

        ListOfMealsRepository()
            .updateMeal(mealToUpdate)
            .then((res) => {
                res.json().then((json) => {
                    if (json[0].element.className !== SUCCESS_VALUE) updateErrors(json);
                    else closeEditMeal();
                })
            });
    }

    const deleteMeal = (meal) => {
        const mealToDelete = {
            'mealid': meal.mealid,
            'username': meal.username
        };

        ListOfMealsRepository()
            .deleteMeal(mealToDelete)
            .then((res) => {
                if (res.status !== 200) {
                    toast.error(intl.formatMessage({id: 'listOfMeals.deleteModalError'}));
                    return;
                    z
                }

                getPagedMeals(currentPageNumber);
                toast.success(intl.formatMessage({id: 'listOfMeals.deleteModalSuccess'}));
            });

        closeDeleteModal();
    }

    const resetMeal = (meal) => {
        const mealToReset = {
            'mealid': meal.mealid,
            'username': meal.username
        };

        ListOfMealsRepository()
            .resetMeal(mealToReset)
            .then((res) => {
                if (res.status !== 200) {
                    toast.error(intl.formatMessage({id: 'listOfMeals.resetModalError'}));
                    return;
                }

                getPagedMeals(currentPageNumber);
                toast.success(intl.formatMessage({id: 'listOfMeals.resetModalSuccess'}));
            });

        closeResetModal();
    }

    const onChangeMealNameModal = (e) => {
        setMealNameModal(e.target.value);
    }

    const onChangeMealTypeModal = (e) => {
        setMealTypeModal(e.target.value);
    }

    const onChangeMealPeriodicityModal = (e) => {
        setMealPeriodicityModal(e.target.value);
    }

    const backToHome = () => {
        toast.dismiss();
        history.push(BACK_TO_HOME);
    }

    const openEditModal = () => {
        setEditModalIsOpen(true);
    }

    const closeEditModal = () => {
        setEditModalIsOpen(false);
    }

    const openResetModal = (meal) => {
        setResetModalIsOpen(true);
        setMealToReset(meal);
    }

    const closeResetModal = () => {
        setResetModalIsOpen(false);
    }

    const openDeleteModal = (meal) => {
        setDeleteModalIsOpen(true);
        setMealToDelete(meal)
    }

    const closeDeleteModal = () => {
        setDeleteModalIsOpen(false);
    }

    const listOfMealModalStyle = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            border: '5px solid #ccc',
            // background: 'red'
        }
    };

    const changeResetSearchImage = (change) => {
        let imageToChange = document.getElementsByClassName("listOfMealResetSearchIcon").item(0);
        if (change) imageToChange.setAttribute('src', IMAGES_URL + 'mealsAppResetIconWhite.png');
        else imageToChange.setAttribute('src', IMAGES_URL + 'mealsAppResetIcon.png');
    }

    const resetSearch = () => {
        let inputsToClean = document.getElementsByClassName("searchInput");
        for (let i = 0; i < inputsToClean.length; i++) {
            if (inputsToClean.item(i).type === "checkbox") inputsToClean.item(i).checked = false;
            else inputsToClean.item(i).value = null;
        }

        let selectToClean = document.getElementsByClassName("searchInputSelect");

        for (let i = 0; i < selectToClean.length; i++) {
            selectToClean.item(i).selected = "none";

            if (selectToClean.item(i).children.item(0).value === "")
                selectToClean.item(i).children.item(0).selected = "selected";
            else
                selectToClean.item(i).children.item(3).selected = "selected";
        }

        setNameFilter("");
        setTypeFilter("");
        setLastDayFilter("");
        setCurrentSizePage(DEFAULT_SIZE_PAGE);
        setEmptyCheckBox(false);
    }

    const setupUsername = () => {
        if (localStorage.getItem('isUserLogged') !== '1') return;
        setUsername(localStorage.getItem('userLoggedName'));
    }

    useEffect(() => {
        checkUserLogged();
        setupUsername();
        getAllMealsInit();
    }, [props.reload]);

    Modal.setAppElement(document.getElementById('root'));

    const renderTitle = () => <div className="listOfMealsBody">
        <p className="listOfMealsName">
            <strong>
                <FormattedMessage id='listOfMeals.title' defaultMessage='LIST OF MEALS'/>
            </strong>
        </p>
    </div>;

    const renderSearchHeader = () => <table className="listOfMealTable">
        <tbody>
        <tr>
            <td className="listOfMealTd">
                <label className={'listOfMealsLabel'}>
                    <FormattedMessage id='listOfMeals.mealName' defaultMessage='Name:'/>
                </label>
                <input className="form-control listOfMealInput mealNameInput searchInput"
                       type="text"
                       placeholder={intl.formatMessage({id: 'listOfMeals.mealNamePlaceholder'})}
                       onChange={onNameFilterInput}/>
            </td>
            <td className="listOfMealTd">
                <label className={'listOfMealsLabel'}>
                    <FormattedMessage id='listOfMeals.mealLastDay' defaultMessage='Last day:'/>
                </label>
                <table className={'listOfMealsLastDayTable'}>
                    <tbody>
                    <tr>
                        <td className={'listOfMealsLastDayInput'}>
                            <input type="date"
                                   className="form-control listOfMealInput listOfMealLastDayInput searchInput"
                                   onChange={onLastDayFilterSelect}/>
                        </td>
                        <td className={'listOfMealsLastDayCheckBox'}>
                            <input className="form-check-input listOfMealCheckbox emptyInput searchInput"
                                   type="checkbox"
                                   onChange={onEmptyCheckBoxChange}
                                   value={emptyCheckBox}/> {emptyCheckBox}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td className="listOfMealTd">
                <label className={'listOfMealsLabel'}>
                    <FormattedMessage id='listOfMeals.mealType' defaultMessage='Type:'/>
                </label>
                <select className="form-control listOfMealSelect mealTypeInput searchInputSelect"
                        onChange={onTypeFilterSelect}>
                    {getTypeOptions().map((option) => (
                        <option value={option.value}>
                            {option.label}
                        </option>))}
                </select>
            </td>
            <td className="listOfMealTd">
                <label className={'listOfMealsLabel'}>
                    <FormattedMessage id='listOfMeals.pageSize' defaultMessage='Page size:'/>
                </label>
                <select className="form-control listOfMealSelect listOfMealPageSizeInput searchInputSelect"
                        onChange={onSizePageChange}
                        defaultValue={DEFAULT_SIZE_PAGE}>
                    {getListOfSizePage().map((sizePage) =>
                        <option key={sizePage} value={sizePage}>
                            {sizePage}
                        </option>)}
                </select>
            </td>
        </tr>
        </tbody>
    </table>;

    const renderSearchButton = () => <table className="listOfMealTable">
        <tbody>
        <tr>
            <td className="listOfMealTd listOfMealSearchTd">
                <button className="btn btn-outline-dark listOfMealButton listOfMealSearchButton"
                        onClick={() => searchByFilter(DEFAULT_PAGE_NUMBER)}>
                    <FormattedMessage id='listOfMeals.searchButton' defaultMessage='Search'/>
                </button>
            </td>
            <td className="listOfMealTd listOfMealResetSearchTd">
                <button className="btn btn-outline-dark listOfMealSearchButton listOfMealResetSearchButton"
                        onMouseOver={() => changeResetSearchImage(true)}
                        onMouseOut={() => changeResetSearchImage(false)}
                        onClick={() => resetSearch()}>
                    <img className="listOfMealResetSearchIcon"
                         src={IMAGES_URL + 'mealsAppResetIcon.png'}
                         alt="mealsAppResetIcon.png"/>
                </button>
            </td>
        </tr>
        </tbody>
    </table>;

    const renderListOfMeals = () => <table className="listOfMealTable">
        <tbody>
        <tr key="header" className="listOfMealTr">
            <th className="listOfMealCenter">
                <FormattedMessage id='listOfMeals.mealNameTable' defaultMessage='NAME'/>
            </th>
            <th className="listOfMealMealType">
                <FormattedMessage id='listOfMeals.mealTypeTable' defaultMessage='TIPO'/>
            </th>
            <th className="listOfMealMealLastDay">
                <FormattedMessage id='listOfMeals.mealPeriodicityTable' defaultMessage='PERIODICIDAD'/>
            </th>
            <th className="listOfMealMealPeriodicity">
                <FormattedMessage id='listOfMeals.mealLastDayTable' defaultMessage='ÚLTIMO DÍA'/>
            </th>
        </tr>
        {allMeals.map((meal) => <tr id={meal.mealid} key={meal.mealid}>
            <td className="listOfMealNormal listOfMealsFontTd">
                <div className="listOfMealTableName">
                    {meal.mealName}
                </div>
            </td>
            <td className="listOfMealMealType listOfMealsFontTd">
                {getFormattedMealType(meal.mealType)}</td>
            <td className="listOfMealMealLastDay listOfMealsFontTd">
                {formatDate(meal.mealLastDay)}
            </td>
            <td className="listOfMealMealPeriodicity listOfMealsFontTd">
                {meal.mealPeriodicity}
            </td>
            <td className="listOfMealTableButtonTd">
                <button id={meal.mealid}
                        className="tableOptions"
                        data-toggle="modal"
                        data-target="#myModal"
                        onClick={() => showEditModalMeal(meal)}>
                    <img className="listOfMealEditIcon" src={IMAGES_URL + 'mealsAppEditIcon.png'}
                         alt="mealsAppEditIcon.png"/>
                </button>
            </td>
            <td className="listOfMealTableButtonTd">
                <button className="tableOptions"
                        onClick={() => openDeleteModal(meal)}>
                    <img className="listOfMealRemoveIcon" src={IMAGES_URL + 'mealsAppRemoveIcon.png'}
                         alt="mealsAppRemoveIcon.png"/>
                </button>
            </td>
            <td className="listOfMealTableButtonTd">
                <button className="tableOptions"
                        onClick={() => openResetModal(meal)}>
                    <img className="listOfMealResetIcon" src={IMAGES_URL + 'mealsAppResetIcon.png'}
                         alt="mealsAppResetIcon.png"/>
                </button>
            </td>
        </tr>)}
        <tr>
            <td colSpan='7' className="listOfMealTd">
                {getPageButtons()}
            </td>
        </tr>
        </tbody>
    </table>;

    const renderBackToHomeButton = () => <table className="listOfMealTable">
        <tbody>
        <tr>
            <td className="listOfMealsBackTd">
                <button className="btn btn-outline-dark listOfMealButton listOfMealBackButton"
                        onClick={() => backToHome()}>
                    <FormattedMessage id='listOfMeals.backButton' defaultMessage='Back'/>
                </button>
            </td>
        </tr>
        </tbody>
    </table>;

    const renderToast = () => <div>
        <Toaster
            position="top-left"
            reverseOrder={false}
            toastOptions={
                {
                    style: {
                        fontSize: '1.4em',
                    }
                }
            }
        />
    </div>;

    function renderEditModal() {
        return <div>
            <Modal
                isOpen={editModalIsOpen}
                onRequestClose={closeEditModal}
                style={listOfMealModalStyle}
                contentLabel="Meal to edit">
                <div className="listOfMealModalTitle">
                    <p className={'listOfMealsModalTitle'}>
                        <strong>
                            <FormattedMessage id='listOfMeals.editModalTitle' defaultMessage='EDIT MODAL'/>
                        </strong>
                    </p>
                </div>
                <table className="listOfMealTable">
                    <tbody>
                    <tr>
                        <td className="listOfMealModalTd">
                            <label className={'listOfMealsModalLabel'}>
                                <FormattedMessage id='listOfMeals.mealNameEditModal' defaultMessage='Name:'/>
                            </label>
                            <input id="mealNameModalInput"
                                   className="form-control listOfMealInput modalNameInput"
                                   type="text"
                                   placeholder={intl.formatMessage({id: 'listOfMeals.mealNamePlaceholderEditModal'})}
                                   onChange={onChangeMealNameModal}
                                   value={mealNameModal}/>
                        </td>
                    </tr>
                    <tr>
                        <td className="listOfMealModalTd">
                            <label className={'listOfMealsModalLabel'}>
                                <FormattedMessage id='listOfMeals.mealTypeEditModal' defaultMessage='Type:'/>
                            </label>
                            <select className="form-control listOfMealSelect modalTypeInput"
                                    onChange={onChangeMealTypeModal}
                                    defaultValue={mealTypeModal}>
                                {getTypeOptions().map((option) => (
                                    <option value={option.value}>
                                        {option.label}
                                    </option>))}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td className="listOfMealModalTd">
                            <label className={'listOfMealsModalLabel'}>
                                <FormattedMessage id='listOfMeals.mealPeriodicityEditModal'
                                                  defaultMessage='Periodicity:'/>
                            </label>
                            <input className="form-control listOfMealInput modalPeriodicityInput"
                                   type="number"
                                   onChange={onChangeMealPeriodicityModal}
                                   value={mealPeriodicityModal}/>
                        </td>
                    </tr>
                    <tr>
                        <td className="listOfMealModalTd listOfMealEditModalButtons">
                            <button className="btn btn-outline-dark listOfMealButton listOfMealModalButton"
                                    onClick={() => updateEditMeal()}>
                                <FormattedMessage id='listOfMeals.saveButtonEditModal' defaultMessage='Save'/>
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td className="listOfMealModalTd listOfMealEditModalButtons">
                            <button className="btn btn-outline-dark listOfMealButton listOfMealModalButton"
                                    onClick={closeEditModal}>
                                <FormattedMessage id='listOfMeals.closeButtonEditModal' defaultMessage='Close'/>
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </Modal>
        </div>;
    }

    const renderResetModal = () => <div>
        <Modal
            isOpen={resetModalIsOpen}
            onRequestClose={closeResetModal}
            style={listOfMealModalStyle}
            contentLabel="Meal to reset">
            <div className="listOfMealModalTitle">
                <p className={'listOfMealsModalTitle'}>
                    <strong>
                        <FormattedMessage id='listOfMeals.resetModalTitle' defaultMessage='RESET MODAL'/>
                    </strong>
                </p>
            </div>
            <div className="listOfMealModalText">
                <p className={'listOfMealsModalText'}>
                    <FormattedMessage id='listOfMeals.resetModalText' defaultMessage='Are you sure to reset '/>
                    <strong>{mealToReset.mealName}</strong>?
                </p>
            </div>
            <table>
                <tbody>
                <tr>
                    <td className="listOfMealResetModalButtons">
                        <button className="btn btn-outline-dark listOfMealResetModalButtons"
                                onClick={() => resetMeal(mealToReset)}>
                            <FormattedMessage id='listOfMeals.resetModalOkButton' defaultMessage='Yes'/>
                        </button>
                    </td>
                    <td className="listOfMealResetModalButtons">
                        <button className="btn btn-outline-dark listOfMealResetModalButtons"
                                onClick={closeResetModal}>
                            <FormattedMessage id='listOfMeals.resetModalCancelButton' defaultMessage='No'/>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </Modal>
    </div>;

    const renderDeleteModal = () => <div>
        <Modal
            isOpen={deleteModalIsOpen}
            onRequestClose={closeDeleteModal}
            style={listOfMealModalStyle}
            contentLabel="Meal to delete">
            <div className="listOfMealModalTitle">
                <p className={'listOfMealsModalTitle'}>
                    <strong>
                        <FormattedMessage id='listOfMeals.deleteModalTitle' defaultMessage='DELETE MODAL'/>
                    </strong>
                </p>
            </div>
            <div className="listOfMealModalText">
                <p className={'listOfMealsModalText'}>
                    <FormattedMessage id='listOfMeals.deleteModalText' defaultMessage='Are you sure to delete '/>
                    <strong>{mealToDelete.mealName}</strong>?
                </p>
            </div>
            <table>
                <tbody>
                <tr>
                    <td className="listOfMealResetModalButtons">
                        <button className="btn btn-outline-dark listOfMealResetModalButtons"
                                onClick={() => deleteMeal(mealToDelete)}>
                            <FormattedMessage id='listOfMeals.deleteModalOkButton' defaultMessage='Yes'/>
                        </button>
                    </td>
                    <td className="listOfMealResetModalButtons">
                        <button className="btn btn-outline-dark listOfMealResetModalButtons"
                                onClick={closeDeleteModal}>
                            <FormattedMessage id='listOfMeals.deleteModalCancelButton' defaultMessage='No'/>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </Modal>
    </div>;

    return (<React.Fragment>

            <div className={'container h-100'}>
                <div className="row h-100 justify-content-center">
                    <div className={'listOfMealsMealHeader'}/>
                    <div className="row justify-content-center align-items-center">
                        {renderTitle()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderSearchHeader()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderSearchButton()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderListOfMeals()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderBackToHomeButton()}
                    </div>
                    <div className={'listOfMealsFooter'}/>
                </div>
            </div>

            {renderToast()}
            {renderEditModal()}
            {renderResetModal()}
            {renderDeleteModal()}

        </React.Fragment>
    );
}
