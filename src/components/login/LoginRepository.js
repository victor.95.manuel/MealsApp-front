import ConstantURLs from '../../api_url'

export default function LoginRepository() {

    const getLoginUser = (userName) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/findUserName?mealLoginRequest=' + userName)
            .then(response => response.json());

    const saveUser = (userToSave) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/user', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(userToSave),
        }).then();

    return {getLoginUser, saveUser}
}
