import React from 'react';
import GetMeal from "./GetMeal.jsx";

export default ({...props}) => <GetMeal {...props} />;
