import React from 'react';
import Home from './Home.jsx';

export default ({...props}) => <Home {...props} />;
