import './CreateMeal.css';
import React, {useState, useEffect} from 'react';
import {useHistory} from "react-router-dom";
import CreateMealRepository from "./CreateMealRepository";
import {toast, Toaster} from "react-hot-toast";
import {FormattedMessage, useIntl} from "react-intl";

export default (props) => {

    const BACK_TO_HOME = "/home"
    const LOGIN = '/'
    const SUCCESS_VALUE = "DomainSuccess";
    const SPANISH_LOCALE = 'es';
    const ENGLISH_LOCALE = 'en';

    const [mealName, setMealName] = useState(null);
    const [mealNameError, setMealNameError] = useState(null);
    const [mealType, setMealType] = useState(null);
    const [mealTypeError, setMealTypeError] = useState(null);
    const [mealPeriodicity, setMealPeriodicity] = useState(null);
    const [mealPeriodicityError, setMealPeriodicityError] = useState(null);
    const [username, setUsername] = useState('');

    const history = useHistory();
    const intl = useIntl();

    useEffect(() => {
        checkUserLogged();
        setupUsername();
    }, [props.reload]);

    const checkUserLogged = () => {
        const isUserLogged = localStorage.getItem('isUserLogged');
        const userLoggedName = localStorage.getItem('userLoggedName');

        if (isUserLogged === null ||
            isUserLogged.length === 0 ||
            isUserLogged === '0' ||
            userLoggedName === null ||
            userLoggedName.length === 0)
            routeToLogin();
    }

    const routeToLogin = () => {
        localStorage.setItem('isUserLogged', '0');
        localStorage.setItem('userLoggedName', '');
        toast.dismiss();

        history.push(LOGIN);
    }

    const getTypeOptions = () => {
        const locale = localStorage.getItem('currentLocaleLanguage');

        if (locale === null || locale.length === 0)
            return [
                {label: "", value: ""},
                {label: "Lunch", value: "LUNCH"},
                {label: "Dinner", value: "DINNER"}
            ];

        switch (locale) {
            case ENGLISH_LOCALE:
                return [
                    {label: "", value: ""},
                    {label: "Lunch", value: "LUNCH"},
                    {label: "Dinner", value: "DINNER"}
                ];
            case SPANISH_LOCALE:
                return [
                    {label: "", value: ""},
                    {label: "Comida", value: "LUNCH"},
                    {label: "Cena", value: "DINNER"}
                ];
            default:
                return [
                    {label: "", value: ""},
                    {label: "Lunch", value: "LUNCH"},
                    {label: "Dinner", value: "DINNER"}
                ];
        }
    }

    const updateErrors = (jsonToUpdate) => {
        if (jsonToUpdate === null) return;

        for (let i = 0; i < jsonToUpdate.length; i++) {
            switch (jsonToUpdate[i].element.className) {
                case "MealName":
                    showMealNameError();
                    break;
                case "MealType":
                    showMealTypeError();
                    break;
                case "MealPeriodicity":
                    showMealPeriodicityError();
                    break;
                case "Meal":
                    showMealAlreadyExists();
                default:
                    break;
            }
        }

        toast.error(intl.formatMessage({id: 'createMeal.error'}));
    }

    const showMealNameError = () => {
        toast.error(intl.formatMessage({id: 'createMeal.mealNameError'}));
        document.getElementsByClassName("mealNameInput").item(0).style.boxShadow = "0 0 0 0.25rem red";

    }

    const showMealTypeError = () => {
        toast.error(intl.formatMessage({id: 'createMeal.mealTypeError'}));
        document.getElementsByClassName("createMealTypeInput").item(0).style.boxShadow = "0 0 0 0.25rem red";
    }

    const showMealPeriodicityError = () => {
        toast.error(intl.formatMessage({id: 'createMeal.mealPeriodicityError'}));
        document.getElementsByClassName("mealPeriodicityInput").item(0).style.boxShadow = "0 0 0 0.25rem red";
    }

    const showMealAlreadyExists = () => {
        toast.error(intl.formatMessage({id: 'createMeal.mealDuplicatedError'}));
        document.getElementsByClassName("mealNameInput").item(0).style.boxShadow = "0 0 0 0.25rem red";
        document.getElementsByClassName("createMealTypeInput").item(0).style.boxShadow = "0 0 0 0.25rem red";
    }

    const cleanErrors = () => {
        setMealNameError(null);
        setMealTypeError(null);
        setMealPeriodicityError(null);

        document.getElementsByClassName("mealNameInput").item(0).style.boxShadow = "none";
        document.getElementsByClassName("createMealTypeInput").item(0).style.boxShadow = "none";
        document.getElementsByClassName("mealPeriodicityInput").item(0).style.boxShadow = "none";
    }

    const cleanCreateMeal = () => {
        let inputsToClean = document.getElementsByClassName("createMealInput");
        for (let i = 0; i < inputsToClean.length; i++)
            inputsToClean.item(i).value = null;

        let selectToClean = document.getElementsByClassName("createMealSelectOptions");
        for (let i = 0; i < selectToClean.length; i++) {
            if (selectToClean.item(i).value === "") selectToClean.item(i).selected = "selected";
        }

        setMealName(null);
        setMealType(null);
        setMealPeriodicity(null);
    }

    const saveFood = () => {
        const mealToSave = {
            'mealName': mealName,
            'mealType': mealType,
            'mealPeriodicity': mealPeriodicity,
            'username': username
        }

        cleanErrors();

        CreateMealRepository()
            .saveMeal(mealToSave)
            .then((response) => {
                response.json().then((json) => {
                    if (json[0].element.className !== SUCCESS_VALUE) updateErrors(json);
                    else {
                        toast.success(intl.formatMessage({id: 'createMeal.mealSuccess'}));
                        cleanCreateMeal();
                    }
                })
            });
    }

    const onMealNameInput = (e) => {
        setMealName(e.target.value);
    }

    const onMealTypeInput = (e) => {
        setMealType(e.target.value);
    }

    const onMealPeriodicityInput = (e) => {
        setMealPeriodicity(e.target.value);
    }

    const backToHome = () => {
        toast.dismiss();
        history.push(BACK_TO_HOME);
    }

    const setupUsername = () => {
        if (localStorage.getItem('isUserLogged') !== '1') return;
        setUsername(localStorage.getItem('userLoggedName'));
    }

    const renderCreateMealTitle = () => <div className="createMealBody">
        <p className="createMealName">
            <strong>
                <FormattedMessage id='createMeal.title' defaultMessage='CREATE MEAL'/>
            </strong>
        </p>
    </div>;

    const renderCreateMealForm = () => <div>
        <table>
            <tbody>
            <tr>
                <td className="createMealTd">
                    <label className={'createMealLabel'}>
                        <FormattedMessage id='createMeal.mealNameLabel' defaultMessage='Name:'/>
                    </label>
                    <input className="form-control createMealInput mealNameInput"
                           placeholder={intl.formatMessage({id: 'createMeal.mealNamePlaceholder'})}
                           onChange={onMealNameInput}/>
                </td>
            </tr>
            <tr>
                <td className="createMealTd">
                    <label className={'createMealLabel'}>
                        <FormattedMessage id='createMeal.mealTypeLabel' defaultMessage='Type:'/>
                    </label>
                    <div>
                        <select className="form-control createMealSelect createMealTypeInput"
                                onChange={onMealTypeInput}>
                            {getTypeOptions().map((option) => (
                                <option className="createMealSelectOptions" value={option.value}>
                                    {option.label}
                                </option>))}
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td className="createMealTd">
                    <label className={'createMealLabel'}>
                        <FormattedMessage id='createMeal.mealPeriodicityLabel' defaultMessage='Periodicity:'/>
                    </label>
                    <input className="form-control createMealInput mealPeriodicityInput"
                           type="number"
                           onChange={onMealPeriodicityInput}/>
                </td>
            </tr>
            </tbody>
        </table>
    </div>;

    const renderCreateMealSaveBackSection = () => <div className="createMealButtonSection">
        <table>
            <tbody>
            <tr>
                <td className="createMealTd">
                    <button className="btn btn-outline-dark createMealButton"
                            onClick={saveFood}>
                        <FormattedMessage id='createMeal.saveButton' defaultMessage='Save'/>
                    </button>
                </td>
            </tr>
            <tr>
                <td className="createMealTd">
                    <button className="btn btn-outline-dark createMealButton"
                            onClick={backToHome}>
                        <FormattedMessage id='createMeal.backButton' defaultMessage='Back'/>
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>;

    const renderCreateMealToast = () => <div>
        <Toaster
            position="top-left"
            reverseOrder={false}
            toastOptions={
                {
                    style: {
                        fontSize: '1.4em',
                    }
                }
            }
        />
    </div>;

    return (<React.Fragment>

            <div className={'container h-100'}>
                <div className="row h-100 justify-content-center">
                    <div className={'createMealHeader'}/>
                    <div className="row justify-content-center align-items-center">
                        {renderCreateMealTitle()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderCreateMealForm()}
                    </div>
                    <div className="row justify-content-center align-items-start">
                        {renderCreateMealSaveBackSection()}
                    </div>
                    <div className={'createMealFooter'}/>
                </div>
            </div>

            {renderCreateMealToast()}

        </React.Fragment>
    );
}
