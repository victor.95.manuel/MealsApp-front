import React from "react";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Login from './components/login/Login.js'
import CreateMeal from './components/createMeal/CreateMeal.js'
import ListOfMeals from './components/listOfMeals/ListOfMeals.js'
import HistoryOfMeals from './components/historyOfMeals/HistoryOfMeals.js'
import Home from './components/home/Home.js'
import GetMeal from './components/getMeal/GetMeal.js'
import './styles.css'

const App = (props) => {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <Login {...props}/>
                </Route>
                <Route exact path="/home">
                    <Home {...props}/>
                </Route>
                <Route exact path="/getMeal">
                    <GetMeal {...props}/>
                </Route>
                <Route exact path="/createMeal">
                    <CreateMeal {...props}/>
                </Route>
                <Route exact path="/listOfMeals">
                    <ListOfMeals {...props}/>
                </Route>
                <Route exact path="/historyOfMeals">
                    <HistoryOfMeals {...props}/>
                </Route>
            </Switch>
        </Router>
    );
};

export default App;
