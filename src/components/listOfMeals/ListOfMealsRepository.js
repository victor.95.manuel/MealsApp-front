import ConstantURLs from '../../api_url'

export default function ListOfMealsRepository() {

    const getAllMeals = (page, size, username) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/findAllPageable?page=' + page + '&size=' + size + '&username=' + username)
            .then(response => response.json());

    const getMealByNameAndTypeAndLastDay = (page, size, mealName, mealType, mealLastDay, emptyCheckBox, username) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/findMealByNameAndTypeAndLastDayPageable?page=' + page + '&size=' + size +
            '&mealName=' + mealName + '&mealType=' + mealType + '&mealLastDay=' + mealLastDay + '&emptyCheckBox=' + emptyCheckBox +
            '&username=' + username)
            .then(response => response.json());

    const updateMeal = (mealToUpdate) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/updateMeal', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(mealToUpdate),
        }).then();

    const deleteMeal = (mealToDelete) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/removeMeal', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(mealToDelete),
        }).then();

    const resetMeal = (mealToReset) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/resetMeal', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(mealToReset),
        }).then();

    return {getAllMeals, getMealByNameAndTypeAndLastDay, updateMeal, deleteMeal, resetMeal}
}