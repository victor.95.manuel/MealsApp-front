import './HistoryOfMeals.css';
import React, {useState, useEffect} from 'react';
import {useHistory} from "react-router-dom";
import HistoryOfMealsRepository from "./HistoryOfMealsRepository";
import {FormattedMessage, useIntl} from "react-intl";
import {toast} from "react-hot-toast";

export default (props) => {

    const DEFAULT_PAGE_NUMBER = 0;
    const DEFAULT_SIZE_PAGE = 4;
    const DEFAULT_PAGE_SIZE = 3;
    const MAX_SIZE_PAGE = 10;
    const BACK_TO_HOME = "/home";
    const LOGIN = "/";
    const IMAGES_URL = '/statics/';

    const [allMeals, setAllMeals] = useState([]);
    const [currentPageNumber, setCurrentPageNumber] = useState(null);
    const [listOfPages, setListOfPages] = useState([]);
    const [nameFilter, setNameFilter] = useState("");
    const [lastDayFilter, setLastDayFilter] = useState("");
    const [currentSizePage, setCurrentSizePage] = useState(DEFAULT_SIZE_PAGE);
    const [username, setUsername] = useState('');

    const history = useHistory();
    const intl = useIntl();

    const checkUserLogged = () => {
        const isUserLogged = localStorage.getItem('isUserLogged');
        const userLoggedName = localStorage.getItem('userLoggedName');

        if (isUserLogged === null ||
            isUserLogged.length === 0 ||
            isUserLogged === '0' ||
            userLoggedName === null ||
            userLoggedName.length === 0)
            routeToLogin();
    }

    const routeToLogin = () => {
        localStorage.setItem('isUserLogged', '0');
        localStorage.setItem('userLoggedName', '');
        toast.dismiss();

        history.push(LOGIN);
    }

    const formatDate = (date) => {
        if (date === null || date.length === 0) return null;

        let dateSplit = formatDateOffset(date).toISOString().split('-')
        let year = dateSplit[0];
        let month = dateSplit[1];
        let day = dateSplit[2].substring(0, 2);

        return day.concat("/").concat(month).concat("/").concat(year);
    }

    const formatDateOffset = (date) => {
        if (date === null || date.length === 0) return null;

        let newDate = new Date(date);
        let timeZoneFromDB = 4.00;
        let timeZoneDifference = timeZoneFromDB * 60 + newDate.getTimezoneOffset();
        return new Date(newDate.getTime() + timeZoneDifference * 60 * 1000);
    }

    const setupPageableList = (pageable) => {
        setAllMeals(pageable.pageableList);
        setCurrentPageNumber(pageable.currentPageNumber + 1);

        let pagesArray = new Array(pageable.pagesNumber);
        let minPageToShow = Math.max(0, pageable.currentPageNumber - DEFAULT_PAGE_SIZE);
        let maxPageToShow = Math.min(pagesArray.length, pageable.currentPageNumber + DEFAULT_PAGE_SIZE);

        for (let i = minPageToShow; i < maxPageToShow; i++)
            pagesArray[i] = i + 1;

        setListOfPages(pagesArray);
    }

    const getButtonClassName = (pageNumber) => {
        if (pageNumber === currentPageNumber) return "historyOfMealCurrentPageNumberButton";
        else return "historyOfMealPageNumberButton";
    }

    const getPagedMeals = (page) => {
        if (nameFilter.length === 0 && lastDayFilter.length === 0)
            return getPageHistoryMeals(page);

        return getPageMealsByNameAndLastDay(page, nameFilter, lastDayFilter);
    }

    const getPageHistoryMeals = (page) => {
        HistoryOfMealsRepository()
            .getHistoryMeals(page - 1, currentSizePage, username)
            .then(setupPageableList);
    }

    const getPageMealsByNameAndLastDay = (page, mealName, mealLastDay) => {
        HistoryOfMealsRepository()
            .getHistoryMealByNameAndLastDay(page - 1, currentSizePage, mealName, mealLastDay, username)
            .then(setupPageableList);
    }

    const getPageButtons = () => {
        return listOfPages.map((page) => <button key={page}
                                                 className={getButtonClassName(page) + " btn btn-outline-dark historyOfMealButton historyOfMealPageButton"}
                                                 onClick={() => getPagedMeals(page)}>
            {page}
        </button>);
    }

    const getHistoryMeals = () => {
        HistoryOfMealsRepository()
            .getHistoryMeals(DEFAULT_PAGE_NUMBER, currentSizePage, username)
            .then(setupPageableList);
    }

    const getHistoryMealsInit = () => {
        if (localStorage.getItem('isUserLogged') !== '1') return;

        const currentUsername = localStorage.getItem('userLoggedName');

        HistoryOfMealsRepository()
            .getHistoryMeals(DEFAULT_PAGE_NUMBER, currentSizePage, currentUsername)
            .then(setupPageableList);
    }

    const searchByFilter = (page) => {
        if (nameFilter.length === 0 && lastDayFilter.length === 0)
            return getHistoryMeals();

        return HistoryOfMealsRepository()
            .getHistoryMealByNameAndLastDay(page, currentSizePage, nameFilter, lastDayFilter, username)
            .then(setupPageableList);
    }

    const onNameFilterInput = (e) => {
        setNameFilter(e.target.value);
    }

    const onLastDayFilterSelect = (e) => {
        setLastDayFilter(e.target.value);
    }

    const getListOfSizePage = () => {
        let arraySizePage = new Array(MAX_SIZE_PAGE);
        for (let i = 0; i < MAX_SIZE_PAGE; i++)
            arraySizePage[i] = i + 1;

        return arraySizePage;
    }

    const onSizePageChange = (e) => {
        setCurrentSizePage(e.target.value);
    }

    const backToHome = () => {
        toast.dismiss();
        history.push(BACK_TO_HOME);
    }

    const changeResetSearchImage = (change) => {
        let imageToChange = document.getElementsByClassName("historyOfMealResetSearchIcon").item(0);
        if (change) imageToChange.setAttribute('src', IMAGES_URL + 'mealsAppResetIconWhite.png');
        else imageToChange.setAttribute('src', IMAGES_URL + 'mealsAppResetIcon.png');
    }

    const resetSearch = () => {
        let inputsToClean = document.getElementsByClassName("searchInput");
        for (let i = 0; i < inputsToClean.length; i++) inputsToClean.item(i).value = null;

        let selectToClean = document.getElementsByClassName("searchInputSelect");

        for (let i = 0; i < selectToClean.length; i++) {
            selectToClean.item(i).selected = "none";
            selectToClean.item(i).children.item(3).selected = "selected";
        }

        setNameFilter("");
        setLastDayFilter("");
        setCurrentSizePage(DEFAULT_SIZE_PAGE);
    }

    const setupUsername = () => {
        if (localStorage.getItem('isUserLogged') !== '1') return;
        setUsername(localStorage.getItem('userLoggedName'));
    }

    useEffect(() => {
        checkUserLogged();
        setupUsername();
        getHistoryMealsInit();
    }, [props.reload]);

    const renderHistoryOfMealsTitle = () => <div className="historyOfMealsBody">
        <p className="historyOfMealsName">
            <strong>
                <FormattedMessage id='historyOfMeals.title' defaultMessage='HISTORY OF MEALS'/>
            </strong>
        </p>
    </div>;

    const renderSearchHeader = () => <table className={'historyOfMealTable'}>
        <tbody>
        <tr>
            <td className="historyOfMealNameInput">
                <label className={'listOfMealsLabel'}>
                    <FormattedMessage id='historyOfMeals.mealName' defaultMessage='Name:'/>
                </label>
                <input className="form-control historyOfMealInput mealNameInput searchInput"
                       type="text"
                       placeholder={intl.formatMessage({id: 'historyOfMeals.mealNamePlaceholder'})}
                       onChange={onNameFilterInput}/>
            </td>
            <td className="historyOfMealLastDayInput">
                <label className={'listOfMealsLabel'}>
                    <FormattedMessage id='historyOfMeals.mealLastDay' defaultMessage='Last day:'/>
                </label>
                <input type="date"
                       className="form-control historyOfMealInput mealLastDayInput searchInput"
                       onChange={onLastDayFilterSelect}/>
            </td>
            <td className="historyOfMealPageSizeInput">
                <label className={'listOfMealsLabel'}>
                    <FormattedMessage id='historyOfMeals.pageSize' defaultMessage='Page size:'/>
                </label>
                <select className="form-control historyOfMealSelect pageSizeInput searchInputSelect"
                        onChange={onSizePageChange}
                        defaultValue={DEFAULT_SIZE_PAGE}>
                    {getListOfSizePage().map((sizePage) =>
                        <option key={sizePage} value={sizePage}>
                            {sizePage}
                        </option>)}
                </select>
            </td>
        </tr>
        </tbody>
    </table>;

    const renderSearchButton = () => <table>
        <tbody>
        <tr>
            <td className="historyOfMealSearchTd">
                <button className="btn btn-outline-dark historyOfMealButton historyOfMealSearchButton"
                        onClick={() => searchByFilter(DEFAULT_PAGE_NUMBER)}>
                    <FormattedMessage id='historyOfMeals.searchButton' defaultMessage='Search'/>
                </button>
            </td>
            <td className="historyOfMealResetSearchTd">
                <button
                    className="btn btn-outline-dark historyOfMealSearchButton historyOfMealResetSearchButton"
                    onMouseOver={() => changeResetSearchImage(true)}
                    onMouseOut={() => changeResetSearchImage(false)}
                    onClick={() => resetSearch()}>
                    <img className="historyOfMealResetSearchIcon"
                         src={IMAGES_URL + 'mealsAppResetIcon'}
                         alt="mealsAppResetIcon.png"/>
                </button>
            </td>
        </tr>
        </tbody>
    </table>;

    const renderHistoryOfMeals = () => <table className="historyOfMealTable">
        <tbody>
        <tr key="header" className="historyOfMealTr">
            <th className="historyOfMealCenter">
                <FormattedMessage id='historyOfMeals.mealNameTable' defaultMessage='NAME'/>
            </th>
            <th className="historyOfMealMealLastDay">
                <FormattedMessage id='historyOfMeals.mealLastDayTable' defaultMessage='DAY'/>
            </th>
        </tr>
        {allMeals.map((meal) => <tr>
            <td className="historyOfMealNormal">
                <div className="historyOfMealTableName">
                    {meal.mealName}
                </div>
            </td>
            <td className="historyOfMealMealLastDay historyOfMealsLastDayTable">
                {formatDate(meal.mealLastDay)}
            </td>
        </tr>)}
        <tr>
            <td colSpan='2' className="">
                {getPageButtons()}
            </td>
        </tr>
        </tbody>
    </table>;

    const renderBackHomeButton = () => <table className="historyOfMealTable">
        <tbody>
        <tr>
            <td className={'listOfMealsBackTd'}>
                <button className="btn btn-outline-dark historyOfMealButton historyOfMealBackButton"
                        onClick={() => backToHome()}>
                    <FormattedMessage id='historyOfMeals.backButton' defaultMessage='Back'/>
                </button>
            </td>
        </tr>
        </tbody>
    </table>;

    return (<React.Fragment>

            <div className={'container h-100'}>
                <div className="row h-100 justify-content-center">
                    <div className={'historyOfMealsMealHeader'}/>
                    <div className="row justify-content-center align-items-center">
                        {renderHistoryOfMealsTitle()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderSearchHeader()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderSearchButton()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderHistoryOfMeals()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderBackHomeButton()}
                    </div>
                    <div className={'historyOfMealsFooter'}/>
                </div>
            </div>

        </React.Fragment>
    );
}
