import ConstantURLs from '../../api_url'

export default function GetMealRepository() {

    const discardMeal = (mealToDiscard) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/discardMeal', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(mealToDiscard),
        }).then();

    const cleanDiscardedMeal = (username) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/cleanDiscardedMeal', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(username)
        }).then();

    const getMealToEat = (mealType, username) =>
        fetch(ConstantURLs.API_URL_CURRENT + '/findMealToEat?mealTypeRequest=' + mealType + '&username=' + username)
            .then(response => response.json());

    return {cleanDiscardedMeal, discardMeal, getMealToEat}
}
