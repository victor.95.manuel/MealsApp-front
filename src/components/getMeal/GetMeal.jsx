import './GetMeal.css';
import React, {useState, useEffect} from 'react';
import {useHistory} from "react-router-dom";
import GetMealRepository from "./GetMealRepository";
import {toast, Toaster} from "react-hot-toast";
import {FormattedMessage} from "react-intl";
import {useIntl} from 'react-intl';

export default (props) => {

    const BACK_TO_HOME = '/home';
    const LOGIN = '/'
    const MEAL_TYPE = ["Lunch", "Dinner"];
    const SUCCESS_VALUE = "DomainSuccess";
    const LUNCH = "Lunch";
    const DINNER = "Dinner";
    const ENGLISH_LOCALE = 'en';
    const SPANISH_LOCALE = 'es';
    const ENGLISH_LUNCH = 'mealsAppLunch.png';
    const ENGLISH_DINNER = 'mealsAppDinner.png';
    const SPANISH_LUNCH = 'mealsAppComida.png';
    const SPANISH_DINNER = 'mealsAppCena.png';
    const IMAGES_URL = '/statics/';

    const [mealTypeToEat, setMealTypeToEat] = useState("");
    const [mealToEat, setMealToEat] = useState("");
    const [mealTypeError, setMealTypeError] = useState("");
    const [username, setUsername] = useState('');

    const history = useHistory();
    const intl = useIntl();

    useEffect(() => {
        checkUserLogged();
        setupUsername();
    }, [props.reload]);

    const checkUserLogged = () => {
        const isUserLogged = localStorage.getItem('isUserLogged');
        const userLoggedName = localStorage.getItem('userLoggedName');

        if (isUserLogged === null ||
            isUserLogged.length === 0 ||
            isUserLogged === '0' ||
            userLoggedName === null ||
            userLoggedName.length === 0)
            routeToLogin();
    }

    const routeToLogin = () => {
        localStorage.setItem('isUserLogged', '0');
        localStorage.setItem('userLoggedName', '');
        toast.dismiss();

        history.push(LOGIN);
    }

    const setupUsername = () => {
        if (localStorage.getItem('isUserLogged') !== '1') return;
        setUsername(localStorage.getItem('userLoggedName'));
    }

    const updateErrors = (jsonToUpdate) => {
        if (jsonToUpdate === null) return;

        jsonToUpdate.map(
            (jsonElement) => {
                const errorToSet = jsonElement.element.error;
                switch (jsonElement.element.className) {
                    case "MealType":
                        setMealTypeError(errorToSet);
                        toast.error(intl.formatMessage({id: 'getMeal.mealTypeError'}));
                        break;
                    case "Meal":
                        toast.error(intl.formatMessage({id: 'getMeal.error'}));
                        break;
                    default:
                        break;
                }
            }
        );

    }

    const cleanErrors = () => {
        setMealTypeError("");
    }

    const getMealToEat = () => {
        cleanErrors();

        GetMealRepository()
            .getMealToEat(mealTypeToEat, username)
            .then((response) => {
                if (response[0] &&
                    response[0].element &&
                    response[0].element.className &&
                    response[0].element.className !== SUCCESS_VALUE)
                    updateErrors(response);
                else {
                    setMealToEat(response.mealName)

                    const mealToEatButtonSection = document.getElementsByClassName("getMealToEatButtonSection");
                    for (let i = 0; i < mealToEatButtonSection.length; i++) {
                        if (mealToEatButtonSection.item(i) === null) continue;
                        mealToEatButtonSection.item(i).style.display = "none";
                    }

                    const mealToEatTextSection = document.getElementsByClassName("getMealToEatTextSection");
                    for (let i = 0; i < mealToEatTextSection.length; i++) {
                        if (mealToEatTextSection.item(i) === null) continue;
                        mealToEatTextSection.item(i).style.display = "block";
                    }

                    const mealToEatResponseSection = document.getElementsByClassName("getMealToEatResponseSection");
                    for (let i = 0; i < mealToEatResponseSection.length; i++) {
                        if (mealToEatResponseSection.item(i) === null) continue;
                        mealToEatResponseSection.item(i).style.display = "block";
                    }
                }
            });
    }

    const onTypeFilterSelect = (e) => {
        setMealTypeToEat(e.target.value);
    }

    const discardMeal = () => {
        hideMealToEat();
        cleanTypeButtonsSections();

        if (mealToEat.length === 0 || mealTypeToEat.length === 0) return;

        const mealToDiscard = {
            'mealName': mealToEat,
            'mealType': mealTypeToEat,
            'username': username
        }

        GetMealRepository()
            .discardMeal(mealToDiscard)
            .then();
    }

    const hideMealToEat = () => {
        let mealToEatButtonSection = document.getElementsByClassName("getMealToEatButtonSection");
        for (let i = 0; i < mealToEatButtonSection.length; i++) {
            if (mealToEatButtonSection.item(i) === null) continue;
            mealToEatButtonSection.item(i).style.display = "block";
        }

        let mealToEatTextSection = document.getElementsByClassName("getMealToEatTextSection");
        for (let i = 0; i < mealToEatTextSection.length; i++) {
            if (mealToEatTextSection.item(i) === null) continue;
            mealToEatTextSection.item(i).style.display = "none";
        }

        let mealToEatResponseSection = document.getElementsByClassName("getMealToEatResponseSection");
        for (let i = 0; i < mealToEatResponseSection.length; i++) {
            if (mealToEatResponseSection.item(i) === null) continue;
            mealToEatResponseSection.item(i).style.display = "none";
        }
    }

    const cleanDiscardedMeals = () => {
        hideMealToEat();
        cleanTypeButtonsSections();

        setMealToEat("");

        GetMealRepository()
            .cleanDiscardedMeal(username)
            .then();
    }

    const backToHome = () => {
        discardMeal();
        toast.dismiss();

        history.push(BACK_TO_HOME);
    }

    const cleanTypeButtonsSections = () => {
        let typeButtonsSection = document.getElementsByClassName("getMealTypeSelection");
        for (let i = 0; i < typeButtonsSection.length; i++) typeButtonsSection.item(i).style.boxShadow = "none";
    }

    const selectTypeButton = (e, value) => {
        if (e === null) return;

        cleanTypeButtonsSections();

        let button = e;

        if (e.target.tagName === "IMG") button = e.target.parentNode;
        button.style.boxShadow = "0 0 0 0.25rem rgb(108 117 125 / 50%)";

        setMealTypeToEat(value);
    }

    const renderGetMealTitle = () => <div className="getMealBody">
        <p className="getMealTitle">
            <strong>
                <FormattedMessage id='getMeal.title' defaultMessage='GET MEAL'/>
            </strong>
        </p>
    </div>;

    const getLocaleLunchImage = () => {
        const locale = localStorage.getItem('currentLocaleLanguage');

        if (locale === null || locale.length === 0)
            return IMAGES_URL + ENGLISH_LUNCH;

        switch (locale) {
            case ENGLISH_LOCALE:
                return IMAGES_URL + ENGLISH_LUNCH;
            case SPANISH_LOCALE:
                return IMAGES_URL + SPANISH_LUNCH;
            default:
                return IMAGES_URL + ENGLISH_LUNCH;
        }
    }

    const getLocaleDinnerImage = () => {
        const locale = localStorage.getItem('currentLocaleLanguage');

        if (locale === null || locale.length === 0)
            return IMAGES_URL + ENGLISH_DINNER;

        switch (locale) {
            case ENGLISH_LOCALE:
                return IMAGES_URL + ENGLISH_DINNER;
            case SPANISH_LOCALE:
                return IMAGES_URL + SPANISH_DINNER;
            default:
                return IMAGES_URL + ENGLISH_DINNER;
        }
    }

    const renderGetMealSection = () => <div>
        <table className="getMealTypeTable">
            <tbody>
            <tr className="trGetMeal">
                <td className="tdGetMeal">
                    <button className="btn btn-outline-secondary getMealTypeSelection"
                            onClick={(button) => selectTypeButton(button, LUNCH)}>
                        <img className="getMealTypeLunchButton" src={getLocaleLunchImage()}
                             alt="mealsAppLunch.png"/>
                    </button>
                </td>
                <td className="tdGetMeal">
                    <button className="btn btn-outline-secondary getMealTypeSelection"
                            onClick={(button) => selectTypeButton(button, DINNER)}>
                        <img className="getMealTypeLunchButton" src={getLocaleDinnerImage()}
                             alt="mealsAppDinner.png"/>
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
        {/*<table className="getMealTable">*/}
        {/*    <tbody className="getMealToEatButtonSection">*/}
        {/*    <tr>*/}
        {/*        <td className="tdGetMeal">*/}
        {/*            <div className="mealToEatButton">*/}
        {/*                <button className="btn btn-outline-dark getMealButton"*/}
        {/*                        onClick={() => getMealToEat()}>*/}
        {/*                    <p className="getMealButtonText">*/}
        {/*                        <FormattedMessage id='getMeal.getButton' defaultMessage='Get'/>*/}
        {/*                    </p>*/}
        {/*                </button>*/}
        {/*            </div>*/}
        {/*        </td>*/}
        {/*    </tr>*/}
        {/*    </tbody>*/}
        {/*</table>*/}
        <table className="getMealTable">
            <tbody className="getMealToEatTextSection">
            <tr className="trGetMeal">
                <td className="tdGetMealFoundMeal">
                    <div id="mealToEat" className="mealToEat">
                        <strong>{mealToEat}</strong>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <table className="getMealTable">
            <tbody className="getMealToEatResponseSection">
            <tr>
                <td className="tdGetMealSection">
                    <div id="mealToEat" className="mealToEat">
                        <button className="btn btn-outline-success getMealButton getMealOkButton"
                                onClick={() => cleanDiscardedMeals()}>
                            <p className="getMealButtonText">
                                <FormattedMessage id='getMeal.okButton' defaultMessage='Ok'/>
                            </p>
                        </button>
                    </div>
                </td>
                <td className="tdGetMealSection">
                    <div id="mealToEat" className="mealToEat">
                        <button className="btn btn-outline-danger getMealButton getMealDiscardButton"
                                onClick={() => discardMeal()}>
                            <p className="getMealButtonText">
                                <FormattedMessage id='getMeal.discardButton' defaultMessage='Discard'/>
                            </p>
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>;

    const renderGetMealBackSection = () => <div className="getMealBackSection">
        <table className="getMealTable">
            <tbody>
            <tr className="trGetMeal">
                <td className="tdGetMeal">
                    <button className="btn btn-outline-dark getMealButton"
                            onClick={() => getMealToEat()}>
                        <p className="getMealButtonText">
                            <FormattedMessage id='getMeal.getButton' defaultMessage='Get'/>
                        </p>
                    </button>
                </td>
            </tr>
            <tr>
                <td className="tdGetMeal">
                    <button className="btn btn-outline-dark getMealButton"
                            onClick={() => backToHome()}>
                        <p className="getMealButtonText">
                            <FormattedMessage id='getMeal.backButton' defaultMessage='Back'/>
                        </p>
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>;

    const renderGetMealToast = () => <div>
        <Toaster
            position="top-left"
            reverseOrder={false}
            toastOptions={
                {
                    style: {
                        fontSize: '1.4em',
                    }
                }
            }
        />
    </div>;

    return (<React.Fragment>

            <div className={'container h-100'}>
                <div className="row h-100 justify-content-center">
                    <div className={'getMealHeader'}/>
                    <div className="row justify-content-center align-items-center">
                        {renderGetMealTitle()}
                    </div>
                    <div className="row justify-content-center align-items-center">
                        {renderGetMealSection()}
                    </div>
                    <div className="row justify-content-center align-items-start">
                        {renderGetMealBackSection()}
                    </div>
                    <div className={'getMealFooter'}/>
                </div>
            </div>

            {renderGetMealToast()}

        </React.Fragment>
    );
}
